#!/usr/bin/env python3
"""
"""

import argparse, configparser, sys, os, feedparser, csv, time, calendar, urllib.parse as urlparse, threading, queue, datetime
import jinja2

feedparserversion = feedparser.USER_AGENT
feedparser.USER_AGENT = "RoidRoll/0.1.0 +https://gitlab.com/puzzlement/roidroll %s" % feedparserversion

CACHE=os.path.join(os.environ.get('HOME'), '.roidrollcache.csv')
VERBOSE=False

FRESH = 1
RECENT = 2
OLD = 3

FRESH_DAYS_DEFAULT = 7
RECENT_DAYS_DEFAULT = FRESH_DAYS_DEFAULT * 4

# Livejournal's indication that it should ask for (digest) auth, which
# we want to strip
LJAUTH = u'auth'
LJAUTHMETHODS = frozenset([u'digest'])

class UnFetchable(Exception):

    def __init__(self, message, url):
        self.url = url
        Exception.__init__(self, message)

def lockPrint(lock, stream, output):
    lock.acquire()
    print (output, file=stream)
    lock.release()

class Cache:

    def __init__(self, cmd_args):
        self.cmd_args = cmd_args
        self.d = {}
        self.lock = threading.Lock()
        self.outputLock = threading.Lock()
        self.readCache()

    def readCache(self):
        try:
            reader = csv.reader(open(CACHE))
        except IOError:
            return
        for fields in reader:
            fields.append(self)
            url = fields[0]
            self.d[url] = CacheItem(self.cmd_args, *fields, outputLock = self.outputLock)

    def get(self, feedurl):
        try:
            return self.d[feedurl]
        except KeyError:
            new = CacheItem(self.cmd_args, feedurl, None, None, None, None, None, None, None, self, self.outputLock)
            self.d[feedurl] = new
            return new

    def store(self):
        writer = open(CACHE, 'w')
        for cacheitem in self.d.values():
            try:
                cacheitem.write(writer, self.lock)
            except UnicodeEncodeError as e:
                lockPrint(self.outputLock, sys.stderr, 'Encoding error for %s, \'%s\' ' % (cacheitem.feedurl, str(e)))

class CacheItem:
    def __init__(self, cmd_args, feedurl, url, title, entrylink, entrytitle, etag, httpModified, timestamp, cache, outputLock):
        self.cmd_args = cmd_args
        self.feedurl, self.url, self.title = feedurl, url, title
        self.entrylink, self.entrytitle = entrylink, entrytitle
        self.outputLock = outputLock
        if self.entrylink or self.entrytitle:
            self.noEntries = False
        else:
            self.noEntries = True
        self.etag, self.httpModified = etag, httpModified
        self.cache = cache
        self.store = True

        try:
            self.updated = datetime.datetime.fromtimestamp(float(timestamp))
        except (ValueError, TypeError):
            self.updated = datetime.datetime.now()

    def setTimeFromStruct(self, httpModified):
        """Set the time using a struct_time as returned by feedparser"""
        # internally we use a string version of the POSIX timestamp
        struct = httpModified
        for format_str in [
                '%a, %d %b %Y %H:%M:%S %Z',
                '%a, %d %b %Y %H:%M:%S %z',
                '%a, %d %b %Y %H:%M:%S',]:
            try:
                struct = time.strptime(httpModified, format_str)
            except TypeError:
                # we probably already have a stuct
                break
            except ValueError as e:
                lockPrint(self.outputLock, sys.stderr, "Error '%s' parsing time '%s' to set cache time for %s, aborting" % (str(e), httpModified, self.feedurl))
            else:
                break
            
        try:
            self.httpModified = str(calendar.timegm(struct))
        except TypeError as e:
            lockPrint(self.outputLock, sys.stderr, "Error '%s' parsing time '%s' to set cache time for %s, aborting" % (str(e), httpModified, self.feedurl))
            raise

    def getTimeAsStruct(self):
        """Get the httpModified time as a struct_time"""
        try:
            return time.gmtime(int(self.httpModified))
        except TypeError as e:
            outputLock(self.outputLock, sys.stderr, "Error '%s' parsing time '%s' retrieving cache time for %s, aborting" % (str(e), self.httpModified, self.feedurl))
            raise

    def write(self, f, lock):
        if not self.store:
            if VERBOSE:
                outputLock(self.outputLock, sys.stderr, "Not storing URL %s, flag set" % self.feedurl)
            return
        row = []
        for item in (self.feedurl, self.url, self.title, self.entrylink,
                self.entrytitle, self.etag, self.httpModified):
            if item == None:
                row.append(u'')
            else:
                row.append(item)

        row.append(str(self.updated.timestamp()))

        lock.acquire()
        writer = csv.writer(f)
        writer.writerow(row)
        lock.release()

    def fetch(self):
        try:
            if self.etag and self.httpModified:
                d = feedparser.parse(self.feedurl, etag=self.etag, modified=self.getTimeAsStruct())
            elif self.etag:
                d = feedparser.parse(self.feedurl, etag=self.etag)
            elif self.httpModified:
                d = feedparser.parse(self.feedurl, modified=self.getTimeAsStruct())
            else:
                d = feedparser.parse(self.feedurl)
            return d
        except Exception as e:
            lockPrint(self.outputLock, "Exception caught inside feedparser for URL %s" % self.feedurl)
            lockPrint(self.outputLock, sys.stderr, "Exception %s" % str(e))
            return {}

    def guessUpdatedTime(self, d):
        try:
            self.updated = datetime.datetime.fromtimestamp(time.mktime(d.entries[0].updated_parsed))
        except (AttributeError, IndexError):
            pass
        else:
            return

        try:
            firstentrylink = d.entries[0].link
            firstentrytitle = d.entries[0].title
        except (AttributeError, IndexError):
            pass
        else:
            if firstentrylink == self.entrylink and firstentrytitle == self.entrytitle:
                # leave self.updated to its previous cached value
                if VERBOSE:
                    lockPrint(self.outputLock, sys.stderr, "Detected old entry on undated feed %s, using timestamp %s" % (self.url, self.updated))
                return
        self.updated = datetime.datetime.now()

    def setProperties(self, d):
        """Set internal properties based on feedparser response d"""

        try:
            self.url = d.feed.link
        except AttributeError:
            raise UnFetchable("Feed %s has no associated URL" % self.feedurl, self.feedurl)
        try:
            self.title = d.feed.title
        except AttributeError:
            self.title = self.url

        self.guessUpdatedTime(d)

        self.noEntries = (len(d.entries) == 0)

        if not self.noEntries:
            # set the first entry details
            try:
                firstentry = d.entries[0]
                self.entrylink = firstentry.link
            except (IndexError, AttributeError):
                self.entrytitle = ''
                self.entrylink = ''
            else:
                try:
                    self.entrytitle = firstentry.title
                except AttributeError:
                    self.entrytitle = self.entrylink

        # set the cache headers
        try:
            self.etag = d.etag
        except AttributeError:
            pass
        try:
            self.setTimeFromStruct(d.modified)
        except (AttributeError, TypeError):
            pass


    def update(self):
        try:
            d = self.fetch()
        except UnicodeDecodeError as e:
            self.store = False
            raise UnFetchable('Error parsing feed %s; \'%s\'' % (self.feedurl, str(e)), self.feedurl)
        try:
            status = d.status
        except AttributeError:
            # remove from cache
            self.store = False
            raise UnFetchable('Feed %s could not be retrieved\n' % self.feedurl, self.feedurl)

        if status == 301:
            lockPrint(self.outputLock, sys.stderr, 'Feed %s has permanently moved to a new URL, update config: %s ' % (self.feedurl, d.href))

        if status == 301 or d.status == 302 or d.status == 307:
            feedurl = d.href
            dest = self.cache.get(feedurl)
            d = dest.update()
            self.setProperties(d)
            if VERBOSE:
                lockPrint(self.outputLock, sys.stderr, "Setting False flag for %s, redirects with %s" % (self.feedurl, d.status))
            self.store = False # only store the eventual destination
        elif status == 304:
            if VERBOSE:
                lockPrint(self.outputLock, sys.stderr, "Using cached version of %s" % self.feedurl)
            pass # use the cache version
        elif status == 410:
            self.store =  False
            raise UnFetchable("Feed %s has been removed, please stop polling" % self.feedurl, self.feedurl)
        elif status == 200:
            self.setProperties(d)

        else:
            self.store = False
            raise UnFetchable('Feed %s has unknown return status %s' %
                    (self.feedurl, status), self.feedurl)
        return d

    def getDateClass(self):
        timesincelastentry = datetime.datetime.now() - self.updated
        if timesincelastentry.days > self.cmd_args.recent:
            dateclass = ("old", OLD)
        elif timesincelastentry.days > self.cmd_args.fresh:
            dateclass = ("recent", RECENT)
        else:
            dateclass = ("fresh", FRESH)
        return dateclass

    def getAge(self):
        timesincelastentry = datetime.datetime.now() - self.updated
        if timesincelastentry.days < 1:
            return "today"
        elif timesincelastentry.days < 7:
            if timesincelastentry.days == 1:
                return "1 day"
            else:
                return "%d days" % timesincelastentry.days
        else:
            weeks = int(timesincelastentry.days/7)
            if weeks == 1:
                return "1 week"
            else:
                return "%d weeks" % weeks

class BadListing(Exception): pass

def getListing(feedurl, cache, parser):
    try:
        authorname = parser.get(feedurl, 'roidname')
    except configparser.NoOptionError:
        try:
            parser.get(feedurl, 'roidusename')
        except configparser.NoOptionError:
            authorname = None
        else:
            try:
                authorname = parser.get(feedurl, 'name')
            except configparser.NoOptionError:
                authorname = None

    strippedfeedurl = stripUserPass(feedurl)

    cacheditem = cache.get(strippedfeedurl)
    cacheditem.update()
    dateclass, primary_sort_key_date = cacheditem.getDateClass()
    age = cacheditem.getAge()

    if cacheditem.noEntries:
        raise BadListing

    if cacheditem.entrylink and cacheditem.entrytitle:
        
        jinja_data = {
            'link': cacheditem.url,
            'title': cacheditem.title,
            'latestentry' : {
                'link': cacheditem.entrylink,
                'title': cacheditem.entrytitle,
            },
        }
        
    elif cacheditem.title:
        jinja_data = {
            'link': cacheditem.url,
            'title': cacheditem.title,
        }
    elif cacheditem.url:
        jinja_data = {
            'link': cacheditem.url,
            'title': cacheditem.url,
        }
    else:
        raise BadListing

    jinja_data['dateclass'] = dateclass
    jinja_data['age'] = age

    if authorname:
        jinja_data['authorname'] = authorname
        secondary_sort_key_title = authorname.lower()
    else:
        secondary_sort_key_title = cacheditem.title.lower()

    return primary_sort_key_date, secondary_sort_key_title, jinja_data
    
def noFetch(feedurl, parser):
    # some feeds don't go in this one
    for property in ('roidnotpublic', 'headline', 'release'):
        try:
            parser.get(feedurl, property)
        except configparser.NoOptionError:
            pass
        else:
            return True
    return False

def stripUserPass(feedurl):
    parse = urlparse.urlparse(feedurl)
    if parse.username is None and parse.password is None:
        return feedurl
    
    # Here follows idiocy, because Python does NOT allow the username
    # and password parameters to be changes in the tuple. So they have
    # to be stripped from the netloc string at location 1

    if parse.port is not None:
        newnetloc = "%s:%s" % (parse.hostname, parse.port)
    else:
        newnetloc = parse.hostname

    newquery = None
    if parse.query is not None:
        queryparams = urlparse.parse_qs(parse.query)
        if LJAUTH in queryparams and queryparams[LJAUTH][0] in LJAUTHMETHODS:
            del queryparams[LJAUTH]
        if queryparams:
            newquery = urllib.urlencode(queryparams)

    newparse = (parse.scheme, newnetloc, parse.path, parse.params,
            newquery, parse.fragment)
    return urlparse.urlunparse(newparse)
        
def worker(q):
    while True:
        d = q.get()
        originalfeedurl, cache, parser = d['url'], d['cache'], d['parser']
        listings = d['listings']
        try:
            age, secondary_sort, jinja_data = getListing(originalfeedurl, cache, parser)
        except UnFetchable as e:
            print(str(e), file=sys.stderr)
        except Exception as e:
            print(str(e), file=sys.stderr)
        except BadListing:
            pass
        else:
            listings.setdefault(age, []).append(
                {'key': secondary_sort, 'jinja': jinja_data})
        q.task_done()

NUMTHREADS=3

TEMPLATE = "roidroll-tmpl.html"

def main():
    parser = argparse.ArgumentParser(
            description="Generate listing of recent blog entries from Planet-style .ini config files")
    parser.add_argument('--fresh', help='Maximum age of most recent entry to '
            'be considered a \'fresh\' (most prominent) blog (default %d)' % FRESH_DAYS_DEFAULT,
            type=int, default = FRESH_DAYS_DEFAULT, metavar='DAYS')
    parser.add_argument('--recent', help='Maximum age of most recent entry to '
            'be considered a \'recent\' (prominent) blog (default %d)' % RECENT_DAYS_DEFAULT,
            type=int, default = RECENT_DAYS_DEFAULT, metavar='DAYS')
    parser.add_argument('templatedir', help="Jinja2 template directory containing %s template file" % TEMPLATE)
    parser.add_argument('outputfile', help="Output file based on %s and your subscriptions" % TEMPLATE)
    parser.add_argument('configfiles', help="Planet-style .ini config files", nargs="+")
    args = parser.parse_args()
    if args.recent <= args.fresh:
        print("Argument to --fresh (%d) must be less than argument to --recent (%d)" % (args.fresh, args.recent), file=sys.stderr)
        sys.exit(1)
    jenv = jinja2.Environment(loader=jinja2.FileSystemLoader(args.templatedir))
    parser = configparser.ConfigParser()
    cache = Cache(args)
    for configfile in args.configfiles:
        parser.readfp(open(configfile))

    sections = parser.sections()
    listings = {}

    q = queue.Queue()
    
    for i in range(NUMTHREADS):
         t = threading.Thread(target=lambda: worker(q))
         t.daemon = True
         t.start()

    for originalfeedurl in sections:
        if originalfeedurl == 'Planet':
            continue

        if noFetch(originalfeedurl, parser):
            continue

        q.put({'url' : originalfeedurl, 'cache' : cache, 'parser': parser, 'listings': listings})

    q.join()

    def blogs():
        for age_key in (FRESH, RECENT, OLD):
            try:
                agelistings = listings[age_key]
            except KeyError:
                continue
            for listing in sorted(agelistings, key = lambda i: i['key']):
                yield listing['jinja']

    template = jenv.get_template(TEMPLATE)
    f = open(args.outputfile, 'w')
    f.write(template.render(blogs = blogs()))
    f.close()
        
    cache.store()

if __name__ == '__main__':
    main()
