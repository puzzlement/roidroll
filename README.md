RoidRoll blogroll generator
===========================

RoidRoll is a script that generates a blog-roll like page in the style of
[Sites Mary Reads](https://files.puzzling.org/reading/).  It is compatible with
[Planet Feed Reader](http://planetplanet.org/) configuration files, allowing you
to publicly share your Planet subscriptions.

There is one key difference between a normal blogroll and a RoidRoll blogroll:
* Normal blogroll: list of blogs or webpages you read.
* RoidRoll blogroll: list of blogs or webpages you read, together with the most recent entry by that writer.

Requirements
=============

* Python 3 (3.2+)
* [feedparser](https://github.com/kurtmckee/feedparser)
* [Jinja2 templating engine](http://jinja.pocoo.org/)

The modules are listed in `requirements.txt` at the root of the repository and can be installed with pip.

Usage
=====

    usage: roidroll.py [-h] [--fresh DAYS] [--recent DAYS]
                       templatedir outputfile configfiles [configfiles ...]
    
    Generate listing of recent blog entries from Planet-style .ini config files
    
    positional arguments:
      templatedir    Jinja2 template directory containing roidroll-tmpl.html
                     template file
      outputfile     Output file based on roidroll-tmpl.html and your
                     subscriptions
      configfiles    Planet-style .ini config files
    
    optional arguments:
      -h, --help     show this help message and exit
      --fresh DAYS   Maximum age of most recent entry to be considered a 'fresh'
                     (most prominent) blog (default 7)
      --recent DAYS  Maximum age of most recent entry to be considered a 'recent'
                     (prominent) blog (default 28)

Configuration
=============

Config files are in the Windows-style .ini format

Using RoidRoll standalone
-------------------------

If you don't use the Planet Feed Reader and you want to use RoidRoll, you want
a config file something like `docs/examples/standalone.ini`

For each site you want to add to the list, do one of the following:

 1. just add the URL of the syndication feed (RSS or Atom) in square brackets
    to your config file:

    [http://example.com/feed.xml]

 2. if you would like to add a particular name for the feed over and above
    what the site calls itself (eg, you want to use the writer's real name):

    [http://example.com/feed2.xml]
    roidname = Your Name Of Choice for this feed

Using RoidRoll on your Planet Feed Reader configs
-------------------------------------------------

RoidRoll is designed to work with the config files also used by the [Planet
Feed Reader](http://planetplanet.org/), so there are some config options that
apply only to those config files.

If you use Planet Feed Reader, you can run RoidRoll over the same config files
(note that it caches its own data, it does not use Planet's or Venus's caches).

There are some extra options you may wish to provide for each feed:

 * if you include a feed in your Planet but do not wish it to appear in your RoidRoll:
   `roidnotpublic=yes`

 * if you want to want to use the same name that Planet uses for each feed:
   `roidusename = yes` (then it will respect the "name=" setting)

 * if you use one name for the feed in your Planet but want to use another in 
   your RoidRoll: `roidname = [new name]`

Example configuration
---------------------

See `docs/examples/standalone.ini` for an example config file.

Templating
==========

RoidRoll output is templated using the Jinja2 templating engine.

The templating engine is provided with a list of items each representing a
blog/subscription from your .ini files, in the template variable `blogs`.

Template variables within each `blog` element of `blogs` are:
* `blog.title`: the title of the blog itself
* `blog.link`: the link to the blog itself
* `blog.age`: a English textual description of the age of the most recent change to the blog, eg "today", "5 days", "3 weeks"
* `blog.dateclass`: a value based on the age of the most recent entry. This can be used as a CSS class value. It has three possible values:
 * `fresh`: has a very recent new entry
 * `recent`: has a recent new entry
 * `old`: does not have a recent entry
* `blog.latestentry` (optional): an object containing two variables with information on the latest entry:
 * `blog.latestentry.link`: a link to the latest entry
 * `blog.latestentry.title`: the title of the latest entry
* `blog.authorname` (optional): the name configured in your Planet .ini files by either
 * the `roidname` variable's value, eg `roidname = Mary`; or
 * the `name` variable value, eg `name = Mary`, combined with `roidusename = yes`

Variables marked "optional" above /may/ be present for any blog, but may not be.

Example template
----------------

See `docs/examples/roidroll-tmpl.html` for an example Jinja2 tempalte file
rendering RoidRoll's template variables as an HTML list.

Features
========

 * Caches data between invocations and respects HTTP Not Modified (304) responses

Credits
=======

RoidRoll was developed by [Mary Gardiner](https://mary.gardiner.id.au/).

Licence
=======

RoidRoll is free software available under the MIT licence. See LICENCE for full
details.
